#include "ChaloEngineProgram.hpp"

#include "../States/MainState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"

#include <string>

std::string ChaloEngineProgram::s_className = "ChaloEngineProgram";
std::string ChaloEngineProgram::s_firstState = "MainState";

ChaloEngineProgram::ChaloEngineProgram( bool fullscreen /* = false */ )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Setup( fullscreen );
    Logger::StackPop();
}

ChaloEngineProgram::~ChaloEngineProgram()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Teardown();
    Logger::StackPop();
}

void ChaloEngineProgram::Setup( bool fullscreen /* = false */ )
{
    Logger::StackPush( s_className + "::" + __func__ );
    std::map<std::string, std::string> options = InitConfig( fullscreen );
    chalo::ChaloProgram::Setup( options );

    InitLanguages();
    InitGlobalAssets();
    InitMenus();
    InitInput();
    InitStates();
    Logger::StackPop();
}

std::map<std::string, std::string> ChaloEngineProgram::InitConfig( bool fullscreen )
{
    Logger::StackPush( s_className + "::" + __func__ );
    std::string full = ( fullscreen ) ? "1" : "0";
    std::map<std::string, std::string> options = {
        std::pair<std::string, std::string>( "CONFIG_NAME", "config.chaloconfig" ),
        std::pair<std::string, std::string>( "TITLEBAR_TEXT", "Millennial Pix" ),
        std::pair<std::string, std::string>( "WINDOW_WIDTH", Helper::ToString( 1280 ) ),
        std::pair<std::string, std::string>( "WINDOW_HEIGHT", Helper::ToString( 720 ) ),
        std::pair<std::string, std::string>( "GAME_WIDTH", "1280" ),
        std::pair<std::string, std::string>( "GAME_HEIGHT", "720" ),
        std::pair<std::string, std::string>( "MENU_PATH", "Content/Menus/" ),
        std::pair<std::string, std::string>( "LANGUAGE_PATH", "Content/Languages/" ),
        std::pair<std::string, std::string>( "FONT_PATH", "Content/Fonts/" ),
        std::pair<std::string, std::string>( "FULLSCREEN", "0" ),
        std::pair<std::string, std::string>( "LANGUAGE_MAIN", "en" ),
        std::pair<std::string, std::string>( "LANGUAGE_MAIN_FONT", "main" ),
        std::pair<std::string, std::string>( "SOUND_VOLUME", "100" ),
        std::pair<std::string, std::string>( "MUSIC_VOLUME", "50" ),
        std::pair<std::string, std::string>( "USE_OPEN_DYSLEXIC", "0" ),
        std::pair<std::string, std::string>( "USE_SUBTITLES", "1" ),
        std::pair<std::string, std::string>( "USE_CAPTIONS", "0" ),
        std::pair<std::string, std::string>( "PLATFORM", "" ),//"PLAYDATE" ),
        std::pair<std::string, std::string>( "PREVENT_JOYSTICK", "1" )      // I think it detects my USB hub as a joystick so this is annoying.
    };

    bool loadedConfig = chalo::ConfigManager::Load( "config.chaloconfig" );

    if ( loadedConfig )
    {
        Logger::Out( "Loaded existing config.", s_className + "::" + __func__ );
        // Load language
        chalo::LanguageManager::SetLanguageBasePath( chalo::ConfigManager::Get( "LANGUAGE_PATH" ) );
        chalo::LanguageManager::AddLanguage(
            chalo::ConfigManager::Get( "LANGUAGE_MAIN" ),
            chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" )
        );

        options.clear();
        options = chalo::ConfigManager::GetOptions();
    }
    else
    {
        Logger::Out( "Couldn't load config, use defaults.", s_className + "::" + __func__ );
        // Set defaults
        for ( auto& option : options )
        {
            chalo::ConfigManager::Set( option.first, option.second );
        }
        chalo::ConfigManager::Save();

        // Load language
        chalo::LanguageManager::SetLanguageBasePath( chalo::ConfigManager::Get( "LANGUAGE_PATH" ) );
        chalo::LanguageManager::AddLanguage(
            chalo::ConfigManager::Get( "LANGUAGE_MAIN" ),
            chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" )
        );
    }
    Logger::StackPop();

    return options;
}

void ChaloEngineProgram::InitLanguages()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // Set up language manager
    chalo::LanguageManager::SetLanguageBasePath( chalo::ConfigManager::Get( "LANGUAGE_PATH" ) );
    chalo::LanguageManager::SetMainLanguage( "en" );
    Logger::StackPop();
}

void ChaloEngineProgram::InitGlobalAssets()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // Set up global assets
    chalo::FontManager::Add( "main", chalo::ConfigManager::Get( "FONT_PATH" ) + "NotoSans-Bold.ttf" );
    chalo::FontManager::Add( "main-od", chalo::ConfigManager::Get( "FONT_PATH" ) + "OpenDyslexic-Bold.otf" );
    Logger::StackPop();
}

void ChaloEngineProgram::InitMenus()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // Set up menu manager
    chalo::MenuManager::Setup( chalo::ConfigManager::Get( "MENU_PATH" ) );
    Logger::StackPop();
}

void ChaloEngineProgram::InitInput()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // Set up input manager
    chalo::InputManager::Setup();
    SetupKeybindings();
    Logger::StackPop();
}

void ChaloEngineProgram::InitStates()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Initialize the state manager",s_className + "::" + __func__ );
    m_stateManager.InitManager();

    // TODO: I still need to fix this up

    m_stateManager.AddState( "MainState", new MainState );

    m_stateManager.ChangeState( s_firstState );

    m_stateManager.Debug();
    Logger::StackPop();
}

void ChaloEngineProgram::Teardown()
{
    Logger::StackPush( s_className + "::" + __func__ );
//    chalo::InputManager::Teardown();
//    chalo::MenuManager::Teardown();
    chalo::ChaloProgram::Teardown();
    Logger::StackPop();
}

void ChaloEngineProgram::Run()
{
    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        chalo::Application::Update();
        m_stateManager.UpdateState();

        if ( m_stateManager.GetGotoState() != "" )
        {
            m_stateManager.ChangeState( m_stateManager.GetGotoState() );
        }

        // Drawing
//        m_stateManager.DrawState( chalo::Application::GetWindow() );
        m_stateManager.DrawState( chalo::Application::GetRenderTexture() );
        chalo::Application::EndDrawing();
    }
}

void ChaloEngineProgram::SetupKeybindings()
{
    Logger::StackPush( s_className + "::" + __func__ );
    // Keybinding setup: Input action type, keyboard key bindings, joystick button bindings, joystick axis bindings
    chalo::InputManager::SetKeybindings( {
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ),
            { sf::Keyboard::W },
            {},
            { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, -100 ) } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ),
            { sf::Keyboard::S },
            {},
            { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, 100 ) } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ),
            { sf::Keyboard::A },
            {},
            { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, -100 ) } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ),
            { sf::Keyboard::D },
            {},
            { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, 100 ) } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ),
            { sf::Keyboard::Q },
            { chalo::JoystickButtonBinding( 0, 0 ), chalo::JoystickButtonBinding( 0, 2 ), chalo::JoystickButtonBinding( 0, 9 ) },
            {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION2 ),
            { sf::Keyboard::E },
            { chalo::JoystickButtonBinding( 0, 1 ), chalo::JoystickButtonBinding( 0, 3 ) },
            {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ),
            { sf::Keyboard::BackSpace },
            { chalo::JoystickButtonBinding( 0, 8 ) },
            {} )
    } );
    Logger::StackPop();
}
