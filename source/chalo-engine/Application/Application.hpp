// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _APPLICATION
#define _APPLICATION

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>

namespace chalo
{

class Application
{
public:
    static void Setup( const std::string& title = "SFML Program",
                      int gameScreenWidth = 400, int gameScreenHeight = 240,
                      int windowWidth = 1280, int windowHeight = 720,
                      bool fullscreen = false, std::string platform = "" );
    static void Teardown();
    static void ChangeDisplay( bool fullscreen, int windowWidth = 1280, int windowHeight = 720 );
    static void AdjustRenderSprite();

    static void BeginDrawing();
    static void EndDrawing();

    static bool IsRunning();
    static void ReadyToQuit();
    static void Update();

    static int GetScreenWidth();
    static int GetScreenHeight();

    static sf::Vector2f GetIntendedScreenDimensions();
    static sf::Vector2f GetScaledScreenDimensions();
    static sf::Vector2f GetWindowDimensions();
    static sf::Vector2f GetScaleRatio();
    static sf::Vector2f GetViewOffset();

    static std::vector<sf::Event>& GetPolledEvents();

    static sf::RenderWindow& GetWindow();
    static sf::RenderTexture& GetRenderTexture();

private:
    static void CheckWindowEvents();

    static sf::RenderWindow m_window;
    static sf::RenderTexture m_renderTexture;
    static sf::Sprite m_renderSprite;
    static bool m_done;
    static sf::Vector2f m_gameScreenDimensionsOriginal;     // e.g., 400x240
    static sf::Vector2f m_gameScreenDimensionsScaled;       // scaled, e.g., 800x480
    static sf::Vector2f m_windowDimensions;                 // size of the actual window, e.g., 1280x720
    static sf::Vector2f m_scaleRatio;                       // how big the original game screen gets scaled
    static sf::Vector2f m_positionOffset;                   // offset, e.g. (1280-800)/2

    static std::vector<sf::Event> m_polledEvents;

    static std::string m_windowTitle;

private:
    static std::string s_className;
};

}

#endif


