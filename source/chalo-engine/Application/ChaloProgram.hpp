// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _CHALO_PROGRAM_HPP
#define _CHALO_PROGRAM_HPP

#include <map>
#include <string>

namespace chalo
{

class ChaloProgram
{
    public:
    void Setup( std::map<std::string,std::string>& options );
    void Teardown();

private:
    static std::string s_className;
};

}

#endif
