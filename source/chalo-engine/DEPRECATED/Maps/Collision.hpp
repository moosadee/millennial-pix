// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _COLLISION_HPP
#define _COLLISION_HPP

#include <string>
#include <SFML/Graphics.hpp>

#include "../GameObjects/GameObject.hpp"

namespace chalo
{

namespace Map
{

//! Collision regions for where the player cannot go
class Collision : public GameObject
{
};

}

}

#endif
