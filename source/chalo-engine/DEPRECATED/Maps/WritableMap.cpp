#include "WritableMap.hpp"

#include "../../Managers/ConfigManager.hpp"

namespace chalo
{

namespace Map
{

void WritableMap::SetMapDimensions( int width, int height )
{
    m_mapWidth = width;
    m_mapHeight = height;
}

void WritableMap::SetTilesets(
    const std::string& tilesetName,
    const std::string& tilesetNameCollisions,
    const std::string& tilesetNameShadows,
    const std::string& tilesetNamePlacements
)
{
    m_tileset = tilesetName;
    m_tilesetCollisions = tilesetNameCollisions;
    m_tilesetShadows = tilesetNameShadows;
    m_tilesetPlacements = tilesetNamePlacements;
}

void WritableMap::PushTilePlacementInfo( TilePlacementInfo info )
{
    Logger::Out( "New tile added... " + info.GetLogString(), "WritableMap::PushTilePlacementInfo" );
    m_lastMapUpdate.push( info );
}

void WritableMap::Reset()
{
    ReadableMap::Reset();
    while ( !m_lastMapUpdate.empty() )
    {
        m_lastMapUpdate.pop();
    }
}

void WritableMap::Undo()
{
    Logger::Out( "Undo, placement stack size: " + Helper::ToString( m_lastMapUpdate.size() )
                 , "WritableMap::Undo" );

    if ( m_lastMapUpdate.size() == 0 )
    {
        return;
    }

    // What's on the top of the stack? Undo it.
    TilePlacementInfo lastChange = m_lastMapUpdate.top();
    m_lastMapUpdate.pop();

    // TODO: Update for above layers, too.
    if ( lastChange.newTile )
    {
        // This basically pops the last tile in the vector
        m_belowTileLayers[ lastChange.lastLayerUpdated ].UndoTile();
    }
    else
    {
        // Change it back to the previous texture
        m_belowTileLayers[ lastChange.lastLayerUpdated ].SetTileTexture( lastChange.tileIndex, lastChange.previousTextureRegion );
    }
}

int WritableMap::GetUndoSize()
{
    return m_lastMapUpdate.size();
}

void WritableMap::RemoveTiles( int layer, sf::IntRect region, LayerType tileType /* = BELOWTILELAYER */ )
{
    Logger::Out(
        " Layer: " + Helper::ToString( layer ) +
        " Region: " + SFMLHelper::RectangleToString( region ) +
        " Layer type: " + Helper::ToString( tileType )
        , "WritableMap::RemoveTiles" );

    // TODO: CLEAN THIS UP!!
    std::list<int> removeTileIndices;

    if ( tileType == BELOWTILELAYER )
    {
        const std::vector<Map::Tile>& tiles = m_belowTileLayers[ layer ].GetTiles();
        for ( int i = 0; i < tiles.size(); i++ )
        {
            if ( SFMLHelper::BoundingBoxCollision( region, tiles[i].GetPositionRegion() ) )
            {
                removeTileIndices.push_front( i );
            }
        }

        // Remove the tiles
        m_belowTileLayers[ layer ].RemoveTiles( removeTileIndices );

    }
    else if ( tileType == ABOVETILELAYER )
    {
        const std::vector<Map::Tile>& tiles = m_aboveTileLayers[ layer ].GetTiles();
        for ( int i = 0; i < tiles.size(); i++ )
        {
            if ( SFMLHelper::BoundingBoxCollision( region, tiles[i].GetPositionRegion() ) )
            {
                removeTileIndices.push_front( i );
            }
        }

        // Remove the tiles
        m_aboveTileLayers[ layer ].RemoveTiles( removeTileIndices );

    }
    else if ( tileType == SHADOWLAYER )
    {
        const std::vector<Map::Shadow>& tiles = m_shadows[ layer ].GetTiles();
        for ( int i = 0; i < tiles.size(); i++ )
        {
            if ( SFMLHelper::BoundingBoxCollision( region, tiles[i].GetPositionRegion() ) )
            {
                removeTileIndices.push_front( i );
            }
        }

        // Remove the tiles
        m_shadows[ layer ].RemoveTiles( removeTileIndices );

    }
    else if ( tileType == COLLISIONLAYER )
    {
        for ( int i = 0; i < m_collisions.size(); i++ )
        {

            Logger::Out(
                "Clear region: " + SFMLHelper::RectangleToString( region ) +
                "Tile region: " + SFMLHelper::RectangleToString( m_collisions[i].GetPositionRegion() ),
                "WritableMap::RemoveTiles" );


            if ( SFMLHelper::BoundingBoxCollision( region, m_collisions[i].GetPositionRegion() ) )
            {
                removeTileIndices.push_front( i );
            }
        }

        Logger::Out( "Total collision tiles to remove: " + Helper::ToString( removeTileIndices.size() ), "WritableMap::RemoveTiles" );

        // Remove the tiles
        for ( auto& removeIndex : removeTileIndices )
        {
            m_collisions.erase( m_collisions.begin() + removeIndex );
        }

    }
        // Clear the undo buffer because now it's janked up
        // could fix this later. TODO. (It references tile indices)
        while ( !m_lastMapUpdate.empty() )
        {
            m_lastMapUpdate.pop();
        }

}

void WritableMap::SaveTextMap( const std::string& mapPath, const std::string& filename )
{
    Logger::Out( "Save map to: " + mapPath + filename, "WritableMap::SaveTextMap" );

    chalo::ConfigManager::Set( "saved-map-" + filename, filename );

    std::ofstream output;
    output.open( mapPath + filename );

    output << "MAP_BEGIN" << std::endl;

    output << " MAP_WIDTH " << m_mapWidth << std::endl;
    output << " MAP_HEIGHT " << m_mapHeight << std::endl;
    output << " TILESET " << m_tileset << std::endl;
    output << " TILESET_COLLISIONS " << m_tilesetCollisions << std::endl;
    output << " TILESET_SHADOWS " << m_tilesetShadows << std::endl;
    output << " TILESET_PLACEMENTS " << m_tilesetPlacements << std::endl;

    for ( auto& belowLayer : m_belowTileLayers )
    {
        output << std::endl;
        output << " LAYER_BEGIN" << std::endl;

        output << "  LAYER_TYPE  " << BELOWTILELAYER << " BELOW_TILE_LAYER" << std::endl;
        output << "  LAYER_INDEX " << belowLayer.first << std::endl;
        output << "  LAYER_NAME  " << belowLayer.second.GetName() << std::endl;

        output << "  LAYER_TILES_BEGIN" << std::endl;

        for ( auto& tile : belowLayer.second.GetTiles() )
        {
            sf::IntRect position = tile.GetPositionRegion();
            sf::IntRect frameRect = tile.GetTextureRegion();

            output << "   TILE_BEGIN"
                   << " TILE_X " << position.left << " TILE_Y " << position.top
                   << " TILE_WIDTH " << position.width << " TILE_HEIGHT " << position.height

                   << " TILE_FRAME_X " << frameRect.left << " TILE_FRAME_Y " << frameRect.top
                   << " TILE_FRAME_WIDTH " << frameRect.width << " TILE_FRAME_HEIGHT " << frameRect.height

                   << " TILE_END"
                   << std::endl;
        }

        output << "  LAYER_TILES_END" << std::endl;

        output << " LAYER_END" << std::endl;
    }

    for ( auto& aboveLayer : m_aboveTileLayers )
    {
        output << std::endl;
        output << " LAYER_BEGIN" << std::endl;

        output << "  LAYER_TYPE  " << ABOVETILELAYER << " ABOVE_TILE_LAYER" << std::endl;
        output << "  LAYER_INDEX " << aboveLayer.first << std::endl;
        output << "  LAYER_NAME  " << aboveLayer.second.GetName() << std::endl;

        output << "  LAYER_TILES_BEGIN" << std::endl;

        for ( auto& tile : aboveLayer.second.GetTiles() )
        {
            sf::IntRect position = tile.GetPositionRegion();
            sf::IntRect frameRect = tile.GetTextureRegion();

            output << "   TILE_BEGIN"
                   << " TILE_X " << position.left << " TILE_Y " << position.top
                   << " TILE_WIDTH " << position.width << " TILE_HEIGHT " << position.height

                   << " TILE_FRAME_X " << frameRect.left << " TILE_FRAME_Y " << frameRect.top
                   << " TILE_FRAME_WIDTH " << frameRect.width << " TILE_FRAME_HEIGHT " << frameRect.height

                   << " TILE_END"
                   << std::endl;
        }

        output << "  LAYER_TILES_END" << std::endl;

        output << " LAYER_END" << std::endl;
    }

    for ( auto& shadow : m_shadows )
    {
        output << std::endl;
        output << " LAYER_BEGIN" << std::endl;

        output << "  LAYER_TYPE  " << SHADOWLAYER << " SHADOW_LAYER" << std::endl;
        output << "  LAYER_INDEX " << shadow.first << std::endl;
        output << "  LAYER_NAME  " << shadow.second.GetName() << std::endl;

        output << "  LAYER_TILES_BEGIN" << std::endl;

        for ( auto& tile : shadow.second.GetTiles() )
        {
            sf::IntRect position = tile.GetPositionRegion();
            sf::IntRect frameRect = tile.GetTextureRegion();

            output << "   TILE_BEGIN"
                   << " TILE_X " << position.left << " TILE_Y " << position.top
                   << " TILE_WIDTH " << position.width << " TILE_HEIGHT " << position.height

                   << " TILE_FRAME_X " << frameRect.left << " TILE_FRAME_Y " << frameRect.top
                   << " TILE_FRAME_WIDTH " << frameRect.width << " TILE_FRAME_HEIGHT " << frameRect.height

                   << " TILE_END"
                   << std::endl;
        }

        output << "  LAYER_TILES_END" << std::endl;

        output << " LAYER_END" << std::endl;
    }

    output << std::endl << " COLLISION_REGIONS_BEGIN" << std::endl;
    for ( auto& collision : m_collisions )
    {
        sf::IntRect position = collision.GetPositionRegion();

        output << "  COLLISION_BEGIN"
            << " COLLISION_X " << position.left
            << " COLLISION_Y " << position.top
            << " COLLISION_TYPE square"
            << " COLLISION_END" << std::endl;
    }
    output << " COLLISION_REGIONS_END" << std::endl;

    output << std::endl << " CHARACTER_PLACEMENTS_BEGIN" << std::endl;
    for ( auto& placement : m_placements )
    {
        sf::IntRect position = placement.GetPositionRegion();

        output  << "   PLACEMENT_BEGIN"
                << " PLACEMENT_X " << position.left
                << " PLACEMENT_Y " << position.top
                << " PLACEMENT_TYPE " << placement.GetCharacterType()
                << " PLACEMENT_END" << std::endl;
    }
    output << " CHARACTER_PLACEMENTS_END" << std::endl;

    output << std::endl << " WARPS_BEGIN" << std::endl;
    for ( auto& warps : m_warps )
    {
    }
    output << " WARPS_END" << std::endl;

    output << "MAP_END" << std::endl;

    output.close();

    // Might as well save the undo stack, too.
    output.open( mapPath + "undostack-" + filename );

    output << "STACK_BEGIN" << std::endl;

    // Gotta reverse the stack
    std::stack<TilePlacementInfo> saveStack;
    std::stack<TilePlacementInfo> copyStack = m_lastMapUpdate;
    while ( !copyStack.empty() )
    {
        saveStack.push( copyStack.top() );
        copyStack.pop();
    }

    while ( !saveStack.empty() )
    {
        output << " ACTION_BEGIN"
               << " ACTION_INDEX " << saveStack.top().actionIndex
               << " LAYER " << saveStack.top().lastLayerUpdated
               << " TILE_INDEX " << saveStack.top().tileIndex
               << " NEWTILE " << saveStack.top().newTile;

        if ( !m_lastMapUpdate.top().newTile )
        {
            // Store the texture region stuff
            output << " TEXTURE_LEFT " << saveStack.top().previousTextureRegion.left
                   << " TEXTURE_TOP " << saveStack.top().previousTextureRegion.top
                   << " TEXTURE_WIDTH " << saveStack.top().previousTextureRegion.width
                   << " TEXTURE_HEIGHT " << saveStack.top().previousTextureRegion.height;
        }

        output << " ACTION_END" << std::endl;

        saveStack.pop();
    }

    output << "STACK_END" << std::endl;

    output.close();
}

void WritableMap::LoadTextMap( const std::string& mapPath, const std::string& filename )
{
    ReadableMap::LoadTextMap( mapPath, filename );

    // Load the undo stack
    std::string buffer;

    std::ifstream input;
    input.open( mapPath + "undostack-" + filename );

    TilePlacementInfo placeInfo;

    while ( input >> buffer )
    {
        if ( buffer == "ACTION_BEGIN" )
        {
        }
        else if ( buffer == "ACTION_END" )
        {
            // Push it
            m_lastMapUpdate.push( placeInfo );
        }
        else if ( buffer == "ACTION_INDEX" )
        {
            input >> placeInfo.actionIndex;
        }
        else if ( buffer == "LAYER" )
        {
            input >> placeInfo.lastLayerUpdated;
        }
        else if ( buffer == "NEWTILE" )
        {
            input >> placeInfo.newTile;
        }
        else if ( buffer == "TILE_INDEX" )
        {
            input >> placeInfo.tileIndex;
        }
        else if ( buffer == "TEXTURE_LEFT" )
        {
            input >> placeInfo.previousTextureRegion.left;
        }
        else if ( buffer == "TEXTURE_TOP" )
        {
            input >> placeInfo.previousTextureRegion.top;
        }
        else if ( buffer == "TEXTURE_WIDTH" )
        {
            input >> placeInfo.previousTextureRegion.width;
        }
        else if ( buffer == "TEXTURE_HEIGHT" )
        {
            input >> placeInfo.previousTextureRegion.height;
        }
    }

    input.close();
}


int WritableMap::GetTileCount( LayerType tileType /* = UNDEFINEDLAYER */ )
{
    int tiles = 0;

    if ( tileType == BELOWTILELAYER || tileType == UNDEFINEDLAYER )
    {
        for ( auto& layer : m_belowTileLayers )
        {
            tiles += layer.second.GetTileCount();
        }
    }

    if ( tileType == ABOVETILELAYER || tileType == UNDEFINEDLAYER )
    {
        for ( auto& layer : m_aboveTileLayers )
        {
            tiles += layer.second.GetTileCount();
        }
    }

    if ( tileType == SHADOWLAYER || tileType == UNDEFINEDLAYER )
    {
        for ( auto& layer : m_shadows )
        {
            tiles += layer.second.GetTileCount();
        }
    }

    if ( tileType == PLACEMENTLAYER || tileType == UNDEFINEDLAYER )
    {
        tiles += m_placements.size();
    }

    if ( tileType == WARPLAYER || tileType == UNDEFINEDLAYER )
    {
        tiles += m_warps.size();
    }

    if ( tileType == COLLISIONLAYER || tileType == UNDEFINEDLAYER )
    {
        tiles += m_collisions.size();
    }

    return tiles;
}

sf::IntRect WritableMap::GetMapRegion()
{
    sf::IntRect region( 0, 0, m_mapWidth, m_mapHeight );
    return region;
}


void WritableMap::AddTile( int layer, Map::Tile& tile, LayerType tileType /* = BELOWTILELAYER */ )
{
    Logger::Out( std::string( "Add new tile " ) +
        " Layer: " + Helper::ToString( layer ) +
        " TileType: " + Helper::ToString( tileType )
        , "WritableMap::AddTile" );

    // Initialize the layer if it doesn't already exist
    if ( tileType == BELOWTILELAYER )
    {
        if ( m_belowTileLayers.find( layer ) == m_belowTileLayers.end() )
        {
            Logger::Out( "Layer \"" + Helper::ToString( layer ) + "\" does not yet exist for below tiles. Init...", "WritableMap::AddTile" );
            m_belowTileLayers[ layer ] = Map::TileLayer();
            m_belowTileLayers[ layer ].SetType( BELOWTILELAYER );
        }
    }
    else if ( tileType == ABOVETILELAYER )
    {
        if ( m_aboveTileLayers.find( layer ) == m_aboveTileLayers.end() )
        {
            Logger::Out( "Layer \"" + Helper::ToString( layer ) + "\" does not yet exist for above tiles. Init...", "WritableMap::AddTile" );
            m_aboveTileLayers[ layer ] = Map::TileLayer();
            m_aboveTileLayers[ layer ].SetType( ABOVETILELAYER );
        }
    }

    // Don't allow duplicate tiles / overwrite the tile if there is already one in this position.
    // TODO: This can be handled more efficiently.
    bool alreadyExists = false;

    std::vector<Tile> existingTiles;

    if ( tileType == ABOVETILELAYER )
    {
        existingTiles = m_belowTileLayers[ layer ].GetTilesUpdatable();
    }
    else if ( tileType == BELOWTILELAYER )
    {
        existingTiles = m_aboveTileLayers[ layer ].GetTilesUpdatable();
    }

    // TODO: UGH DUPLICATE CODE but I don't hvae time to do this right. :|

    Logger::Out( "Total existing tiles: " + Helper::ToString( existingTiles.size() ), "WritableMap::AddTile" );

    for ( int i = 0; i < existingTiles.size(); i++ )
    {
        Logger::Out(
            " Existing tile position: " + SFMLHelper::CoordinateToString( existingTiles[i].GetPosition() ) +
            " New tile position: " + SFMLHelper::CoordinateToString( tile.GetPosition() )
            , "" );
        if ( existingTiles[i].GetPosition() == tile.GetPosition() )
        {
            alreadyExists = true;

            // Is this the same image??? Then ignore.
            if ( existingTiles[i].GetTextureRegion() == tile.GetTextureRegion() )
            {
                continue;
            }
            else
            {
                // Just change this tile's image
                sf::IntRect originalRect = existingTiles[i].GetTextureRegion();
                sf::IntRect textureRect = tile.GetTextureRegion();
                existingTiles[i].SetTextureRegion( textureRect );

                if ( tileType == ABOVETILELAYER )
                {
                    PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, ABOVETILELAYER, i, false, originalRect ) );
                }
                else
                {
                    PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, BELOWTILELAYER, i, false, originalRect ) );
                }
            }
        }
    }

    if ( !alreadyExists && tileType == ABOVETILELAYER  )
    {
        m_aboveTileLayers[ layer ].AddTile( tile );
        PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, ABOVETILELAYER, -1, true ) );
    }
    if ( !alreadyExists && tileType == BELOWTILELAYER )
    {
        m_belowTileLayers[ layer ].AddTile( tile );
        PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, BELOWTILELAYER, -1, true ) );
    }

}



void WritableMap::AddShadow( int layer, Map::Shadow tile )
{

    if ( m_shadows.find( layer ) == m_shadows.end() )
    {
        Logger::Out( "Layer \"" + Helper::ToString( layer ) + "\" does not yet exist for shadow tiles. Init...", "WritableMap::AddTile" );
        m_shadows[ layer ] = Map::ShadowLayer();
        m_shadows[ layer ].SetType( SHADOWLAYER );
    }

    std::vector<Map::Shadow>& existingTiles = m_shadows[ layer ].GetTilesUpdatable();

    bool alreadyExists = false;
    for ( int i = 0; i < existingTiles.size(); i++ )
    {
        if ( existingTiles[i].GetPosition() == tile.GetPosition() )
        {
            alreadyExists = true;

            // Is this the same image??? Then ignore.
            if ( existingTiles[i].GetTextureRegion() == tile.GetTextureRegion() )
            {
                continue;
            }
            else
            {
                // Just change this tile's image
                sf::IntRect originalRect = existingTiles[i].GetTextureRegion();
                sf::IntRect textureRect = tile.GetTextureRegion();
                existingTiles[i].SetTextureRegion( textureRect );

                PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, SHADOWLAYER, i, false, originalRect ) );
            }
        }
    }


    if ( !alreadyExists )
    {
        Logger::Out( "Creating new shadow tile", "WritableMap::AddTile" );
        Map::Shadow shadow( tile );
        m_shadows[ layer ].AddTile( shadow );
        PushTilePlacementInfo( TilePlacementInfo( m_lastMapUpdate.size(), layer, SHADOWLAYER, -1, true ) );
    }
}

void WritableMap::AddCollision( Map::Collision collision )
{
    for ( int i = 0; i < m_collisions.size(); i++ )
    {
        if ( m_collisions[i].GetPosition() == collision.GetPosition() )
        {
            return;
        }
    }

    m_collisions.push_back( collision );
}

void WritableMap::AddWarp( Map::Warp warp )
{
    m_warps.push_back( warp );
}

void WritableMap::AddPlacement( Map::Placement placement )
{
    // Check if this already exists
    for ( auto& item : m_placements )
    {
        if ( item.GetPosition() == placement.GetPosition() )
        {
            return;
        }
    }
    m_placements.push_back( placement );
}

}
}
