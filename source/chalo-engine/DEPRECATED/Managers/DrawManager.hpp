// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _DRAW_MANAGER
#define _DRAW_MANAGER

#include <SFML/Graphics.hpp>

#include <vector>
#include <map>

#include "../../Managers/MenuManager.hpp"
#include "../../Utilities/Logger.hpp"
//#include "../GameObjects/GameObject.hpp"
//#include "../GameObjects/Tile.hpp"
#include "../Maps/ReadableMap.hpp"
#include "../../UI/UILabel.hpp"
#include "../../UI/UIImage.hpp"
#include "../../UI/UIButton.hpp"
#include "../../UI/UIShape.hpp"

namespace chalo
{

struct DrawLayer
{
    DrawLayer()
    {
        name = "unnamed";
    }
    DrawLayer( const std::string& name )
    {
        this->name = name;
    }

    std::string name;
    std::vector<sf::Sprite> images;
};

class DrawManager
{
public:
    static void Setup();
    static void SetDebugView( bool value );
    static bool GetDebugView();
    static void ToggleDebugView();

    // Game objects
    static void AddGameObject( const GameObject& object );
    static void AddMap( const Map::ReadableMap& map, int currentLayer = -1, LayerType layerType = UNDEFINEDLAYER );
    static void AddBelowMapSprite( const Map::Tile& tile );
    static void AddAboveMapSprite( const Map::Tile& tile );
    static void AddHudImage( int layer, const sf::Sprite& sprite ); // Replacing old AddUIImage
    static void AddHudText( const sf::Text& text ); // Replacing old AddUIText

    // UI stuff
    static void AddUIImage( const UIImage& image );
    static void AddUILabel( const UILabel& label );
    static void AddUIButton( const UIButton& button );
    static void AddUIRectangle( const UIRectangleShape& shape );
    static void AddUITextBox( const UITextBox& textbox );

    static void AddDebugText( const sf::Text& text );
    static void AddDebugRectangle( const sf::RectangleShape& shape );

    static void AddMenu();

    // Misc
    static void AddSprite( const sf::Sprite& sprite );
    static void AddCursor( const sf::Sprite& sprite );

    static void SetBackgroundColor( sf::Color color );
    static sf::Color GetBackgroundColor();

    static void Draw( sf::RenderWindow& window );
    static void Reset();

private:
    static bool m_debugView;
    static sf::Color m_backgroundColor;

    // Just a plain-ole sprite
    static std::vector<sf::Sprite> m_sprites;

    // Cursor
    static std::vector<sf::Sprite> m_cursor;

    // Objects need to be drawn based on their 'y' coordinates.
    // The keys here will be the "y" value, and then each "y" value has
    // a vector of images to draw.
    static std::map<int, DrawLayer> m_objectLayers;

    // This vector doesn't get updated often and stores the map draw data
    // in order so that the correct layers are drawn below
    static std::map<int, DrawLayer> m_belowLayers;
    static std::map<int, DrawLayer> m_aboveLayers;

    // HUD drawing (replacing old ui sprites/text)
    static std::map<int, DrawLayer> m_hudSprites;
    static std::vector<sf::Text> m_hudText;

    // UI drawing
    static std::vector<UIImage> m_uiImages;
    static std::vector<UILabel> m_uiLabels;
    static std::vector<UIButton> m_uiButtons;
    static std::vector<UIRectangleShape> m_uiRectangles;
    static std::vector<UITextBox> m_uiTextBoxes;

    // Debug
    static std::vector<sf::Text> m_debugText;
    static std::vector<sf::RectangleShape> m_debugRectangles;
};

}

#endif
