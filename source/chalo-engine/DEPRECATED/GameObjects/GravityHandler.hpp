#ifndef _GRAVITY_HANDLER
#define _GRAVITY_HANDLER

namespace chalo
{

class GravityHandler
{
    public:
    GravityHandler();

    //! Retrieve whether the object is currently falling
    bool GetIsFalling();
    //! Set whether the object is currently falling
    void SetIsFalling( bool value );

    //! Get the gravity value
    float GetGravity();
    //! Set the gravity value
    void SetGravity( float value );

    protected:
    bool m_isFalling;
    float m_gravity;
};

}

#endif
