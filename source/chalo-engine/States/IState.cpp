// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "IState.hpp"
#include "../Utilities/Logger.hpp"

#include <string>

namespace chalo
{

std::string IState::s_className = "IState";

void IState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_name = name;
    m_gotoState = "";
    Logger::StackPop();
}

void IState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_gotoState = "";
    Logger::StackPop();
}

std::string IState::GetName()
{
    return m_name;
}

std::string IState::GetGotoState()
{
    return m_gotoState;
}

void IState::SetGotoState( const std::string& stateName )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_gotoState = stateName;
    Logger::StackPop();
}

}
