// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _AUDIO_MANAGER
#define _AUDIO_MANAGER

#include <SFML/Audio.hpp>

#include <map>
#include <string>

namespace chalo
{

class AudioManager
{
public:
    static void AddMusic( const std::string& key, const std::string& path );
    static void AddSoundBuffer( const std::string& key, const std::string& path );

    static void Clear();

    static const sf::Music& GetMusic( const std::string& key );
    static const sf::SoundBuffer& GetSoundBuffer( const std::string& key );

    static void PlayMusic( const std::string& key, bool repeat = true );
    static void PlaySound( const std::string& key );

private:
    static std::map<std::string, sf::Music> m_music;
    static std::map<std::string, sf::SoundBuffer> m_soundBuffer;

private:
    static std::string s_className;
};

}

#endif
