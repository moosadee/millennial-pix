#include "LanguageManager.hpp"

#include <string>
#include <fstream>

#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"
#include "ConfigManager.hpp"

namespace chalo
{

std::string LanguageSet::s_className = "LanguageSet";

void LanguageSet::Add( const std::string& key, const std::string& value )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_textMap[ key ] = value;
    Logger::StackPop();
}

std::string LanguageSet::Text( const std::string& textKey )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Get text with textKey " + textKey, s_className + "::" + __func__ );
    if ( m_textMap.find( textKey ) == m_textMap.end() )
    {
        Logger::Error( "Could not find text string with key \"" + textKey + "\"!", s_className + "::" + __func__ );
        Logger::StackPop();
        return "ERROR";
    }
    Logger::StackPop();
    return m_textMap[ textKey ];
}

void LanguageSet::SetSuggestedFont( const std::string& font )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_suggestedFont = font;
    Logger::StackPop();
}

std::string LanguageSet::GetSuggestedFont()
{
    return m_suggestedFont;
}


std::string LanguageManager::s_className = "LanguageManager";

std::string LanguageManager::s_basePath;
std::string LanguageManager::s_currentLanguage;
std::map< std::string, LanguageSet > LanguageManager::s_languageSets;

void LanguageManager::SetLanguageBasePath( const std::string& path )
{
    Logger::StackPush( s_className + "::" + __func__ );
    s_basePath = path;
    Logger::StackPop();
}

void LanguageManager::AddLanguage( const std::string& languageKey, const std::string& suggestedFont )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Add language \"" + languageKey + "\"", s_className + "::" + __func__ );
    std::string path = s_basePath + languageKey + ".chalolang";
    std::string key;
    std::string text;
    std::string space;

    std::ifstream input( path );

    if ( input.fail() )
    {
        Logger::Error( "Unable to open language file \"" + path + "\"", s_className + "::" + __func__ );
        Helper::DisplayDirectoryContents( s_basePath );
        return;
    }

    s_languageSets[ languageKey ].SetSuggestedFont( suggestedFont );

    /*
    startup_gametitle
    Chalo Engine Template Game

    startup_pickin_sticks_demo
    Pickin' Sticks demo
    */
    while ( getline( input, key ) )
    {
        getline( input, text );
        getline( input, space );

        s_languageSets[ languageKey ].Add( key, text );
    }
    Logger::StackPop();
}

void LanguageManager::SetMainLanguage( const std::string& languageKey )
{
    Logger::StackPush( s_className + "::" + __func__ );
    s_currentLanguage = languageKey;
    Logger::StackPop();
}

std::string LanguageManager::Text( const std::string& textKey )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Get text with textKey " + textKey, s_className + "::" + __func__ );
    Logger::StackPop();
    return s_languageSets[ s_currentLanguage ].Text( textKey );
}

std::string LanguageManager::Text( const std::string& languageKey, const std::string& textKey )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Get text with textKey \"" + textKey + "\" with languageKey \"" + languageKey + "\"", s_className + "::" + __func__ );
    if ( s_languageSets.find( languageKey ) == s_languageSets.end() )
    {
        Logger::Error( "Could not find language with key \"" + languageKey + "\"!", s_className + "::" + __func__ );
        Logger::StackPop();
        return "ERROR";
    }
    Logger::StackPop();
    return s_languageSets[ languageKey ].Text( textKey );
}

}

