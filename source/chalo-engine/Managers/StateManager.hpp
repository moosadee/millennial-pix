// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _STATE_MANAGER
#define _STATE_MANAGER

#include <map>
#include "../States/IState.hpp"

namespace chalo
{

class StateManager
{
public:
    static void InitManager();
    static void Cleanup();

    static void AddState( const std::string& stateName, IState* state );
    static void ChangeState( const std::string& stateName );
    static void SetupState();
    static void UpdateState();
//    static void DrawState( sf::RenderWindow& window );
    static void DrawState( sf::RenderTexture& window );
    static std::string GetGotoState();
    static void Debug();
    static void FATALERROR();

private:
    static std::map< std::string, IState* > m_states;  // TODO: Update to new ptr
    static IState* m_ptrCurrentState;                  // TODO: Update to new ptr
    static IState* m_defaultState;
    static std::string m_currentStateName;

private:
    static std::string s_className;
};

}

#endif
