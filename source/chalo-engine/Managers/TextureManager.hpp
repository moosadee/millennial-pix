// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _TEXTURE_MANAGER
#define _TEXTURE_MANAGER

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

namespace chalo
{

class TextureManager
{
public:
    static void Setup();
    static void Add( const std::string& key, const std::string& path );
    static const sf::Texture& AddAndGet( const std::string& key, const std::string& path );
    static void Clear();
    static const sf::Texture& Get( const std::string& key );

private:
    static std::map<std::string, sf::Texture> m_assets;

private:
    static std::string s_className;
};

}

#endif
