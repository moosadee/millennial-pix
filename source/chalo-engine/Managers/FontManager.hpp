// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _FONT_MANAGER
#define _FONT_MANAGER

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

namespace chalo
{

class FontManager
{
public:
    static void Add( const std::string& key, const std::string& path );
    static void Clear();
    static sf::Font& Get( const std::string& key );

private:
    static std::map<std::string, sf::Font> m_assets;

private:
    static std::string s_className;
};

}

#endif
