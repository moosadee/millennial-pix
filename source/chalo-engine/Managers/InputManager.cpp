// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "InputManager.hpp"
#include "../Application/Application.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"
#include "ConfigManager.hpp"

#include <string>

namespace chalo
{

std::string InputManager::s_className = "InputManager";

int  InputManager::m_keyboardCooldown;
int  InputManager::m_mouseClickCooldown;
int  InputManager::m_cooldownMax;
int  InputManager::m_mouseCooldownMax;
bool InputManager::m_joystickEnabled[4];
int InputManager::m_gamepadCooldown[4];     // TODO: Actually implement this
int InputManager::m_gamepadCooldownMax[4];
std::map<std::string, InputBinding> InputManager::m_bindings;
bool InputManager::m_mouseLeftPressed;

void InputManager::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_keyboardCooldown = 0;
    m_mouseClickCooldown = 0;
    m_cooldownMax = 7;
    m_mouseCooldownMax = 10;
    m_mouseLeftPressed = false;

    for ( int i = 0; i < 4; i++ )
    {
        m_gamepadCooldown[i] = 0;
        m_gamepadCooldownMax[i] = 7;
        if ( sf::Joystick::isConnected( i ) && chalo::ConfigManager::GetInt( "PREVENT_JOYSTICK" ) == 0 )
        {
            m_joystickEnabled[i] = true;
            Logger::Out( "Joystick " + Helper::ToString( i ) + " enabled", s_className + "::" + __func__ );
        }
        else
        {
            m_joystickEnabled[i] = false;
        }
    }
    Logger::StackPop();
}

void InputManager::SetKeybinding( InputBinding binding )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_bindings[ binding.action.name ] = binding;
    Logger::StackPop();
}

void InputManager::SetKeybindings( std::vector<InputBinding> bindings )
{
    Logger::StackPush( s_className + "::" + __func__ );
    for ( auto& binding : bindings )
    {
        SetKeybinding( binding );
    }
    Logger::StackPop();
}

bool InputManager::IsActionActive( PlayerInputAction action )
{
    Logger::StackPush( s_className + "::" + __func__ );
    // This action has been bound
    if ( m_bindings.find( action.name ) != m_bindings.end() )
    {
        // Are any of the keys being pressed?
        for ( auto& key : m_bindings[ action.name ].keys )
        {
            if ( IsKeyPressedSmooth( key ) )
            {
                Logger::StackPop();
                return true;
            }
        }

        // Joystick buttons
        if ( m_joystickEnabled[ action.index ] )
        {
            for ( auto& button : m_bindings[ action.name ].buttons )
            {
                if ( IsJoystickButtonPressed( button.index, button.button ) )
                {
                    Logger::StackPop();
                    return true;
                }
            }

            // Joystick axes
            for ( auto& axis : m_bindings[ action.name ].axises )
            {
                if ( IsJoystickDpadPressed( axis.index, axis.axis, axis.value ) )
                {
                    Logger::StackPop();
                    return true;
                }
            }
        }
    }
    Logger::StackPop();
    return false;
}

bool InputManager::IsJoystickAvailable( int index )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::StackPop();
    return IsJoystickEnabled( index );
}

void InputManager::Update()
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( m_keyboardCooldown > 0 )
    {
        m_keyboardCooldown -= 1;
    }

    if ( m_mouseClickCooldown > 0 )
    {
        m_mouseClickCooldown -= 1;
    }

    // Check for mouse button activity
    IsLeftClick();
    Logger::StackPop();
}

void InputManager::DisableKeyboard()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_keyboardCooldown = m_cooldownMax;
    Logger::StackPop();
}

void InputManager::DisableMouse()
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_keyboardCooldown = m_cooldownMax;
    Logger::StackPop();
}

bool InputManager::IsKeyboardEnabled()
{
    return ( m_keyboardCooldown <= 0 );
}

bool InputManager::IsMouseEnabled()
{
    return ( m_mouseClickCooldown <= 0 );
}

bool InputManager::IsJoystickEnabled( int index )
{
    return ( index >= 0 && index < 4 && m_joystickEnabled[index] );
}

bool InputManager::IsKeyPressed( sf::Keyboard::Key keyType )
{
    if ( IsKeyboardEnabled() && sf::Keyboard::isKeyPressed( keyType ) )
    {
        DisableKeyboard();
        return true;
    }

    return false;
}

bool InputManager::IsKeyPressedSmooth( sf::Keyboard::Key keyType )
{
    return ( IsKeyboardEnabled() && sf::Keyboard::isKeyPressed( keyType ) );
}

/**
Knockoff SNES controller: "X" = 0, "A" = 1, "B" = 2, "Y" = 3,
"START" = 9, "SELECT" = 8, "LEFT" = 4, "RIGHT" = 5;
Gamepad axis has -100 or 100 (no analog stick).
*/
bool InputManager::IsJoystickButtonPressed( int index, int button )
{
    return ( index >= 0 && index < 4 && sf::Joystick::isButtonPressed( index, button ) );
}

bool InputManager::IsJoystickDpadPressed( int index, sf::Joystick::Axis axis, int value )
{
    if ( index < 0 || index >= 4 ) { return false; }

    return ( sf::Joystick::getAxisPosition( index, axis ) == value );
}

bool InputManager::IsJoystickDpadPressed( int index, Direction dir )
{
    if ( index < 0 || index >= 4 ) { return false; }

    if ( dir == NORTH )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::Y ) == -100 );
    }
    else if ( dir == SOUTH )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::Y ) == 100 );
    }
    else if ( dir == WEST )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::X ) == -100 );
    }
    else if ( dir == EAST )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::X ) == 100 );
    }

    return false;
}

std::string InputManager::GetTypedLetter()
{
    if ( !IsKeyboardEnabled() )
    {
        return "";
    }

    // Kludgey method for now
    std::string typed;

    if      ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )           { typed =  "a"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::B ) )           { typed =  "b"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::C ) )           { typed =  "c"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )           { typed =  "d"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::E ) )           { typed =  "e"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F ) )           { typed =  "f"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::G ) )           { typed =  "g"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::H ) )           { typed =  "h"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::I ) )           { typed =  "i"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::J ) )           { typed =  "j"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::K ) )           { typed =  "k"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::L ) )           { typed =  "l"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::M ) )           { typed =  "m"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::N ) )           { typed =  "n"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::O ) )           { typed =  "o"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::P ) )           { typed =  "p"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Q ) )           { typed =  "q"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::R ) )           { typed =  "r"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )           { typed =  "s"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::T ) )           { typed =  "t"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::U ) )           { typed =  "u"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::V ) )           { typed =  "v"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )           { typed =  "w"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::X ) )           { typed =  "x"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Y ) )           { typed =  "y"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Z ) )           { typed =  "z"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num0 ) )        { typed =  "0"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num1 ) )        { typed =  "1"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num2 ) )        { typed =  "2"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num3 ) )        { typed =  "3"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num4 ) )        { typed =  "4"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num5 ) )        { typed =  "5"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num6 ) )        { typed =  "6"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num7 ) )        { typed =  "7"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num8 ) )        { typed =  "8"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num9 ) )        { typed =  "9"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad0 ) )     { typed =  "0"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad1 ) )     { typed =  "1"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad2 ) )     { typed =  "2"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad3 ) )     { typed =  "3"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad4 ) )     { typed =  "4"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad5 ) )     { typed =  "5"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad6 ) )     { typed =  "6"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad7 ) )     { typed =  "7"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad8 ) )     { typed =  "8"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad9 ) )     { typed =  "9"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Subtract ) )    { typed =  "-"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Dash ) )        { typed =  "-"; }

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::LShift ) || sf::Keyboard::isKeyPressed( sf::Keyboard::RShift ) )
    {
        typed = Helper::ToUpper( typed );
    }

    if ( typed != "" )
    {
        DisableKeyboard();
    }

    return typed;
}

bool InputManager::IsLeftClick()
{
    if ( m_mouseClickCooldown <= 0 && sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_mouseLeftPressed = true;
        return true;
    }
    return false;
}

bool InputManager::IsLeftClickRelease()
{
    if ( m_mouseLeftPressed && !sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_mouseLeftPressed = false;
        return true;
    }
    return false;
}

void InputManager::DisableClick()
{
    m_mouseClickCooldown = m_mouseCooldownMax;
}

sf::Vector2f InputManager::GetMousePosition()
{
    sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );

    // World coordinates
    sf::Vector2f worldPosition = Application::GetWindow().mapPixelToCoords( localPosition );

    // Adjust from screen scale
    sf::Vector2f scaleRatio = chalo::Application::GetScaleRatio();
    // Adjust from if there's borders around the screen due to the resizing
    sf::Vector2f offset = chalo::Application::GetViewOffset();

    sf::Vector2f adjustedPosition( (worldPosition.x / scaleRatio.x) - offset.x, (worldPosition.y / scaleRatio.y) - offset.y );

    return adjustedPosition;
}

}
