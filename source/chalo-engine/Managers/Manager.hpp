/*
 * #ifndef _MANAGER
#define _MANAGER

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

#include "../Utilities/Logger.hpp"

template <typename T>
class Manager<T>
{
    public:
    void Add( const std::string& key, const std::string& path );
    void Clear();
    T& Get( const std::string& key );

    protected:
    std::map<std::string, T> m_assets;
};


std::map<std::string, sf::Texture> TextureManager::m_textures;

void Manager::Add( const std::string& key, const std::string& path )
{
    Logger::Out( "Adding asset with key \"" + key + "\" to path \"" + path + "\"", "TextureManager::AddTexture" );
    T asset;
    if ( !texture.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load texture at path \"" + path + "\"", "TextureManager::AddTexture" );
        return;
    }

    m_textures[ key ] = texture;
}

void Manager::Clear()
{
    m_assets.clear();
}

sf::Texture& Manager::GetTexture( const std::string& key )
{
    return m_textures[ key ];
}

#endif
*/
