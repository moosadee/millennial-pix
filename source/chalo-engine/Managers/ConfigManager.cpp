#include "ConfigManager.hpp"

#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"

#include <fstream>
#include <string>

namespace chalo
{

std::string ConfigManager::s_className = "ConfigManager";

std::map< std::string, std::string > ConfigManager::m_configValues;
std::string ConfigManager::m_configPath;

bool ConfigManager::Load( std::string filename )
{
    Logger::StackPush( s_className + "::" + __func__ );

    Logger::Out( "Config load", s_className + "::" + __func__ );
    m_configPath = filename;
    std::ifstream input( m_configPath );

    if ( !input.good() )
    {
        Logger::Out( "Could not find config file... create new one.", s_className + "::" + __func__ );
        Create();
        Logger::StackPop();
        return false;
    }

    std::string buffer;
    while ( getline( input, buffer ) )
    {
        buffer = Helper::Trim( buffer );
        if ( buffer == "SETTING_BEGIN" )
        {
            std::string key, value;
            getline( input, key );
            getline( input, value );
            Init( key, value );
        }
    }

    input.close();

    Save();

    Logger::StackPop();
    return true;
}

void ConfigManager::Save()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Config save", s_className + "::" + __func__ );
    std::ofstream output( m_configPath );

    for ( auto& setting : m_configValues )
    {
        output << "SETTING_BEGIN" << std::endl
            << setting.first << std::endl
            << setting.second << std::endl
            << "SETTING_END" << std::endl << std::endl;
    }

    output.close();
    Logger::StackPop();
}

std::map< std::string, std::string > ConfigManager::GetOptions()
{
    return m_configValues;
}

void ConfigManager::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Save();
    Logger::StackPop();
}

void ConfigManager::Create()
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Config create", s_className + "::" + __func__ );
    std::ofstream output( m_configPath );
    output.close();
    Logger::StackPop();
}

std::string ConfigManager::Get( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Config get setting with key \"" + key + "\"", s_className + "::" + __func__ );
    if ( m_configValues.find( key ) != m_configValues.end() )
    {
        // Found
        Logger::StackPop();
        return m_configValues[ key ];
    }
    Logger::StackPop();
    return "";
}

int ConfigManager::GetInt( const std::string& key )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Config get setting with key \"" + key + "\"", s_className + "::" + __func__ );
    std::string value = Get( key );
    Logger::StackPop();
    return Helper::StringToInt( value );
}

std::map< std::string, std::string > ConfigManager::GetPartial( const std::string& partialKey )
{
    Logger::StackPush( s_className + "::" + __func__ );
    std::map< std::string, std::string > matchingOptions;

    for ( auto& option : m_configValues )
    {
        if ( Helper::Contains( option.first, partialKey ) )
        {
            matchingOptions[ option.first ] = option.second;
        }
    }
    Logger::StackPop();
    return matchingOptions;
}

void ConfigManager::Init( const std::string& key, const std::string& value )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Init config item. Key: \"" + key + "\", value: \"" + value + "\"", s_className + "::" + __func__ );
    m_configValues[ key ] = value;
    Logger::StackPop();
}

void ConfigManager::Set( const std::string& key, const std::string& value )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Set config item. Key: \"" + key + "\", value: \"" + value + "\"", s_className + "::" + __func__ );
    m_configValues[ key ] = value;
    Save();
    Logger::StackPop();
}

void ConfigManager::Set( const std::string& key, int value )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Set( key, Helper::ToString( value ) );
    Logger::StackPop();
}


};
