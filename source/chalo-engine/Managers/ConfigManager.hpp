// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _CONFIG_MANAGER_HPP
#define _CONFIG_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <map>

namespace chalo
{

class ConfigManager
{
public:
    static bool Load( std::string filename );
    static void Save();
    static void Cleanup();

    static std::string Get( const std::string& key );
    static int GetInt( const std::string& key );
    static std::map< std::string, std::string > GetPartial( const std::string& partialKey );
    static void Init( const std::string& key, const std::string& value );
    static void Set( const std::string& key, const std::string& value );
    static void Set( const std::string& key, int value );
    static std::map< std::string, std::string > GetOptions();

private:
    static std::map< std::string, std::string > m_configValues;
    static std::string m_configPath;

    static void Create();

private:
    static std::string s_className;
};

}

#endif
