#include "ScreenDrawer.hpp"

#include <iostream>
#include <sstream>
using namespace std;

const int ScreenDrawer::SCREEN_WIDTH = 80;
const int ScreenDrawer::SCREEN_HEIGHT = 20;
char ScreenDrawer::s_grid[80][20];

void ScreenDrawer::Init()
{
    Clear();
}

void ScreenDrawer::Clear()
{
	for (int y = 0; y < SCREEN_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			s_grid[x][y] = ' ';
		}
	}
}

void ScreenDrawer::Clear(int x, int y)
{
	Set(x, y, ' ');
}

void ScreenDrawer::Set(int x, int y, char symbol)
{
	if (IsValidRange(x, y))
	{
		s_grid[x][y] = symbol;
	}
}

void ScreenDrawer::Set(int x, int y, int number)
{
	// Convert int to char
	ostringstream oss;
	oss << number;
	Set(x, y, oss.str());
}

void ScreenDrawer::Set(int x, int y, string str)
{
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (i >= SCREEN_WIDTH) { break; }
		Set(x+i, y, str[i]);
	}
}

void ScreenDrawer::Draw()
{
	ClearConsole();
	for (int y = 0; y < SCREEN_HEIGHT;y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			cout << s_grid[x][y];
		}
		cout << endl;
	}
	cout << endl;
}

int ScreenDrawer::GetScreenWidth()
{
	return SCREEN_WIDTH;
}

int ScreenDrawer::GetScreenHeight()
{
	return SCREEN_HEIGHT;
}

void ScreenDrawer::ClearConsole()
{
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		system("cls");
	#else
		system("clear");
	#endif
}

bool ScreenDrawer::IsValidRange(int x, int y)
{
	return (x >= 0 && x < SCREEN_WIDTH&& y >= 0 && y < SCREEN_HEIGHT);
}
