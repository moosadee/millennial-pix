#ifndef _HELPER_HPP
#define _HELPER_HPP

#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <locale>
#include <codecvt>
using namespace std;

class Helper
{
    private:
    static std::string s_className;

    /***************************************************************/
    /* User input functions ****************************************/
    public:
    static int      GetIntInput();
    static float    GetFloatInput();
    static char     GetCharInput();
    static string   GetStringInput();
    static string   GetStringLine();
    static int      GetIntInput( int min, int max );

    private:
    static bool     s_needCinIgnore;

    /***************************************************************/
    /* Menu functionality ******************************************/
    public:
    static void ClearScreen();
    static void Pause();
    static void Header( const string& header );
    static void DrawHorizontalBar( int width, char symbol = '-' );
    static void ShowMenu( const vector<string>& options, bool vertical = true, bool startAtOne = true );
    static int  ShowMenuGetInt( const vector<string>& options, bool vertical = true, bool startAtOne = true );

    /***************************************************************/
    /* System info *************************************************/
    public:
    static void PrintPwd();
    static void Sleep( int milliseconds );
    static void DisplayDirectoryContents( string path );
    static string GetTime();

    /***************************************************************/
    /* Conversion functionality ************************************/
    public:
    template <typename T>
    static string   ToString( const T& value );
    static int      StringToInt( const string& str );
    static float    StringToFloat( const string& str );
    static string   CurrencyToString( float currency );
    static string   ToUpper( const string& val );
    static string   ToLower( const string& val );
    static string   CoordinateToString( int x, int y );
    static wstring  StringToWString( string text );
    static string   WStringToString( std::wstring text );

    /***************************************************************/
    /* String helpers **********************************************/
    public:
    static bool     Contains( string haystack, string needle, bool caseSensitive = true );
    static int      Find( string haystack, string needle );
    static string   Replace( string fulltext, string findme, string replacewith );
    static vector<string> Split( string str, string delim );
    static string   Trim( string original );
    static void     Test_Trim();

    /***************************************************************/
    /* Array helpers ***********************************************/
    public:
    template <typename T>
    static void     ArrayOutput( T myArray[], int size, ofstream& out );
    template <typename T>
    static string   ArrayToString( T myArray[], int size );

    template <typename T>
    static void     VectorOutput( vector<T> myVector, ofstream& out );
    template <typename T>
    static string   VectorToString( vector<T> myVector );

    /***************************************************************/
    /* Timer functionality *****************************************/
    public:
    static void   ClockStart();
    static size_t GetElapsedSeconds();
    static size_t GetElapsedMilliseconds();

    private:
    static chrono::system_clock::time_point s_startTime;

    /***************************************************************/
    /* Random functionality ****************************************/
    public:
    static void SeedRandomNumberGenerator();
    static int  GetRandom( int min, int max );

    /***************************************************************/
    /* Math functionality ******************************************/
    static bool BoundingBoxCollision( int obj1x, int obj1y, int obj1width, int obj1height, int obj2x, int obj2y, int obj2width, int obj2height );
    static bool PointInBoxCollision( int pointX, int pointY, int boxX, int boxY, int boxWidth, int boxHeight );
    static float GetDistance( int left1, int top1, int width1, int height1, int left2, int top2, int width2, int height2, bool fromCenter );
    static float GetDistance( int x1, int y1, int x2, int y2 );
    static float DotProduct( float x1, float y1, float x2, float y2 );
    static float Length( float x, float y );
    static float AngleBetweenTwoPointsRadians( float x1, float y1, float x2, float y2 );
    static float AngleBetweenTwoPointsDegrees( float x1, float y1, float x2, float y2 );

    // For use to check if mouse is within an object
//    bool PointInBoxCollision( sf::Vector2f point, sf::IntRect box );
    // For use to check if mouse is within an object
//    bool PointInBoxCollision( sf::Vector2i point, sf::IntRect box );

//    float GetDistance( sf::IntRect a, sf::IntRect b, bool fromCenter );
//
//    float GetDistance( sf::Vector2f a, sf::Vector2f b );
//
//    float DotProduct( sf::Vector2f a, sf::Vector2f b );
//
//    float Length( sf::Vector2f vec );
//
//    float AngleBetweenTwoPointsRadians( sf::Vector2f a, sf::Vector2f b );
//
//    float AngleBetweenTwoPointsDegrees( sf::Vector2f a, sf::Vector2f b );

};


template <typename T>
string Helper::ToString( const T& value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}


template <typename T>
void Helper::ArrayOutput( T myArray[], int size, ofstream& out )
{
    for ( int i = 0; i < size; i++ )
    {
        if ( i != 0 ) { cout << ", "; }
        out << myArray[i];
    }
    out << endl;
}

template <typename T>
string Helper::ArrayToString( T myArray[], int size )
{
    string text = "";

    for ( int i = 0; i < size; i++ )
    {
        if ( i != 0 ) { text += ", "; }
        text += ToString( myArray[i] );
    }

    return text;
}

template <typename T>
void     Helper::VectorOutput( vector<T> myVector, ofstream& out )
{
    for ( size_t i = 0; i < myVector.size(); i++ )
    {
        if ( i != 0 ) { cout << ", "; }
        out << myVector[i];
    }
    out << endl;
}

template <typename T>
string   Helper::VectorToString( vector<T> myVector )
{
    string text = "";

    for ( size_t i = 0; i < myVector.size(); i++ )
    {
        if ( i != 0 ) { text += ", "; }
        text += ToString( myVector[i] );
    }

    return text;
}

#endif
