#ifndef _TMX_MAP_HPP
#define _TMX_MAP_HPP

#include "../Utilities/CsvParser.hpp"
#include "../Utilities/XmlParser.hpp"
#include <string>

struct TmxMapLayer
{
    int id;
    int width;
    int height;
    std::string name;
    std::vector<int> data;
};

struct TmxTileset
{
    std::string tsxSource;
    std::string imgSource;
    int firstGid;
    int width;
    int height;
    int tileCount;
    int columns;
    int tileWidth;
    int tileHeight;
    int imageWidth;
    int imageHeight;
};

class TmxMap
{
public:
    void Load( std::string path, std::string mapFile );
    void Debug();

private:
    static std::string s_className;

    std::string m_mapVersion;
    std::string m_tiledVersion;
    int m_width;
    int m_height;
    int m_tileWidth;
    int m_tileHeight;
    std::vector<TmxTileset> m_tilesets;
    std::vector<TmxMapLayer> m_layers;

    friend class ChaloMap;
};

#endif
