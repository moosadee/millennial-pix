#include "ChaloMap.hpp"

#include "../Managers/TextureManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"

std::string ChaloMap::s_className = "ChaloMap";

void ChaloMap::Clear()
{
    m_layers.clear();
    m_objects.clear();
    m_objectTypes.clear();
    m_solidTileRegions.clear();
}

void ChaloMap::ConvertFrom( const TmxMap& tmxMap )
{
    Logger::StackPush( s_className + "::" + __func__ );

    // Load in types of objects information
    CsvDocument objectTypesDoc = CsvParser::Parse( "Content/Maps/sheet-objecttypes.csv" );
    for ( auto& row : objectTypesDoc.rows )
    {
        if ( row.size() >= 2 )
        {
            m_objectTypes[ Helper::StringToInt( row[0] ) ] = row[1];
        }
    }

    Logger::DebugValue( "tmxMap.m_tileWidth", Helper::ToString( tmxMap.m_tileWidth ), s_className + "::" + __func__ );
    Logger::DebugValue( "tmxMap.m_tileHeight", Helper::ToString( tmxMap.m_tileHeight ), s_className + "::" + __func__ );
    Logger::DebugValue( "tmxMap.m_width", Helper::ToString( tmxMap.m_width ), s_className + "::" + __func__ );
    Logger::DebugValue( "tmxMap.m_height", Helper::ToString( tmxMap.m_height ), s_className + "::" + __func__ );

    m_tileDimensions.x = tmxMap.m_tileWidth;
    m_tileDimensions.y = tmxMap.m_tileHeight;
    m_mapDimensions.x = tmxMap.m_width;
    m_mapDimensions.y = tmxMap.m_height;

    // Figure out spritesheet offsets; terrain can be used as-is,
    // but objects are special items and require special loading.
    int terrainTileOffset = 0;
    int objectTileOffset = 0;
    TmxTileset terrainTileset;
    TmxTileset objectTileset;
    for ( auto& tileset : tmxMap.m_tilesets )
    {
        if ( Helper::Contains( tileset.tsxSource, "object", false ) )
        {
            objectTileOffset = tileset.firstGid;
            objectTileset = tileset;
        }
        else //if ( Helper::Contains( tileset.tsxSource, "terrain", false ) )
        {
            terrainTileOffset = tileset.firstGid;
            terrainTileset = tileset;
            m_tilesetName = tileset.imgSource;
        }
    }

    const sf::Texture& txTileset = chalo::TextureManager::AddAndGet( "tileset", "Content/Graphics/Tilesets/" + m_tilesetName );
    chalo::TextureManager::Add( "objectset", "Content/Graphics/Tilesets/" + m_objectsetName );

    for ( auto& layer : tmxMap.m_layers )
    {
        if ( Helper::Contains( layer.name, "object", false )
//            || Helper::Contains( layer.name, "wall", false )
            )
        {
            // Object placement layer
            for ( size_t index = 0; index < layer.data.size(); index++ )
            {
                ChaloMapObjectData obj;

                obj.position.x = ( index % layer.width ) * tmxMap.m_tileWidth;
                obj.position.y = ( index / layer.width ) * tmxMap.m_tileHeight;

                if ( Helper::Contains( layer.name, "object", false ) )
                {
                    obj.tmxId = layer.data[index] - objectTileOffset;
                    if ( obj.tmxId == 0 )
                    {
                        continue;
                    }

                    if ( m_objectTypes.find( obj.tmxId ) == m_objectTypes.end() )
                    {
                        // No tile name found; skip it.
                        continue;
                    }

                    obj.name = m_objectTypes[ obj.tmxId ];
                }
//                else if ( Helper::Contains( layer.name, "wall", false ) )
//                {
//                    obj.tmxId = layer.data[index];
//                    if ( obj.tmxId == 0 )
//                    {
//                        continue;
//                    }
//
//                    obj.name = "wall";
//                }

                m_objects.push_back( obj );
            }
        }
        else
        {
            ChaloMapLayer chaloLayer;

            // Terrain layer
            int zIndex = layer.id;
            chaloLayer.name = layer.name;
            chaloLayer.mapDimensions = sf::IntRect( 0, 0, tmxMap.m_width, tmxMap.m_height );

            for ( size_t index = 0; index < layer.data.size(); index++ )
            {
                ChaloTile newTile;

                newTile.m_dimensions.left   = 0;
                newTile.m_dimensions.top    = 0;
                newTile.m_dimensions.width  = tmxMap.m_tileWidth;
                newTile.m_dimensions.height = tmxMap.m_tileHeight;

                // Position of the tile on the map
                newTile.m_tmxIndex = index;
                newTile.m_position.x = (index % layer.width) * newTile.m_dimensions.width;
                newTile.m_position.y = (index / layer.width) * newTile.m_dimensions.height;

                // Position of the tile's coordinates on the tileset
                int tileId = layer.data[index] - terrainTileOffset;
                newTile.m_tmxTileId = tileId;
                int tileX = tileId % terrainTileset.columns;
                int tileY = tileId / terrainTileset.columns;
                newTile.m_frameRect.left    = tileX * newTile.m_dimensions.width;
                newTile.m_frameRect.top     = tileY * newTile.m_dimensions.height;
                newTile.m_frameRect.width   = newTile.m_dimensions.width;
                newTile.m_frameRect.height  = newTile.m_dimensions.height;

                newTile.m_sprite.setTexture( txTileset );
                newTile.m_sprite.setTextureRect( newTile.m_frameRect );
                newTile.m_sprite.setPosition( newTile.m_position );

                if ( chaloLayer.name == "FLOOR" )
                {
                    newTile.m_canWalkOn = true;
                }
                else
                {
                    newTile.m_canWalkOn = false;
                }

                // TODO: Put this in an external file
                if ( (tileX >= 0 || tileX <= 4) && tileY == 1 ) { newTile.m_name = "Floor" + Helper::ToString( tileX ); }

                if ( newTile.m_frameRect.left < 0 || newTile.m_frameRect.top < 0 )
                {
                    if ( chaloLayer.name == "FLOOR" )
                    {
                        Logger::Out( "SKIPPING " + chaloLayer.name + " TILE " + Helper::ToString( index )
                            + ", TILEID: " + Helper::ToString( tileId )
                            + ", FRAME: " + SFMLHelper::RectangleToString( newTile.m_frameRect )
                            + ", TILEPOS: " + SFMLHelper::CoordinateToString( newTile.m_position )
                        );
                    }
                    // Empty tile
                    continue;
                }

                if ( newTile.m_canWalkOn == false )
                {
                    m_solidTileRegions.push_back( newTile.GetCollisionRegion() );
                }

                chaloLayer.tiles.push_back( newTile );
            }

            m_layers.push_back( chaloLayer );
        }
    }

    Logger::StackPop();
}

std::vector<sf::IntRect>& ChaloMap::GetSolidTileRegions()
{
    return m_solidTileRegions;
}

std::vector<ChaloMapObjectData> ChaloMap::GetObjectData()
{
    return m_objects;
}

std::vector<ChaloTile> ChaloMap::GetSolidWalls()
{
    for ( auto& layer : m_layers )
    {
        if ( Helper::Contains( layer.name, "wall", false ) )
        {
            return layer.tiles;
        }
    }

    return std::vector<ChaloTile>();
}

std::string ChaloMap::GetTilesetName()
{
    return m_tilesetName;
}

void ChaloMap::Draw( sf::RenderTexture& window )
{
    Logger::StackPush( s_className + "::" + __func__ );

    for ( auto& layer : m_layers )
    {
        for ( auto& tile : layer.tiles )
        {
            window.draw( tile.m_sprite );
        }
    }

    #ifdef DEBUG
    DrawDebug( window );
    #endif

    Logger::StackPop();
}

void ChaloMap::DrawDebug( sf::RenderTexture& window )
{
//    std::vector<sf::IntRect> solidTileRegions = GetSolidTileRegions();
//
//    for ( auto& tile : solidTileRegions )
//    {
//        sf::RectangleShape rect;
//        rect.setFillColor( sf::Color::Transparent );
//        rect.setOutlineThickness( 1 );
//        rect.setPosition( sf::Vector2f( tile.left, tile.top ) );
//        rect.setSize( sf::Vector2f( tile.width, tile.height ) );
//        rect.setOutlineColor( sf::Color::Yellow );
//
//        window.draw( rect );
//    }

}

void ChaloMap::Debug()
{
    Logger::StackPush( s_className + "::" + __func__ );

    std::string debugInfo = "Tileset name: \"" + m_tilesetName + "\"<br>"
        + "m_tileDimensions:" + SFMLHelper::CoordinateToString( m_tileDimensions ) + "<br>"
        + "m_mapDimensions:" + SFMLHelper::CoordinateToString( m_mapDimensions ) + "<br>";

    for ( auto& layer : m_layers )
    {
        debugInfo += "LAYER: " + layer.name + "<br>";

        for ( auto& tile : layer.tiles )
        {
            debugInfo += "... TILE NAME: " + tile.m_name + ", ";
            debugInfo += "TMX INDEX: " + Helper::ToString( tile.m_tmxIndex ) + ", ";
            debugInfo += "TMX TILE ID: " + Helper::ToString( tile.m_tmxTileId ) + ", ";
            debugInfo += "POS: (" + SFMLHelper::CoordinateToString( tile.m_sprite.getPosition() )  + "), ";
            debugInfo += "FRAME: (" + SFMLHelper::RectangleToString( tile.m_sprite.getTextureRect() )  + "), ";
            debugInfo += "DIMENSIONS: (" + SFMLHelper::RectangleToString( tile.m_dimensions )  + "), ";
            debugInfo += "<br>";
        }

        debugInfo += "<br>";
    }

    for ( size_t i = 0; i < m_objects.size(); i++ )
    {
        debugInfo += "OBJECT #" + Helper::ToString( i );
        debugInfo += ", TMX ID: " + Helper::ToString( m_objects[i].tmxId );
        debugInfo += ", NAME: " + m_objects[i].name;
        debugInfo += ", POS: (" + SFMLHelper::CoordinateToString( m_objects[i].position ) + ")";
        debugInfo += "<br>";
    }

    Logger::Debug( debugInfo, s_className + "::" + __func__ );

    Logger::StackPop();
}

sf::Vector2i ChaloMap::GetMapDimensions()
{
    return m_mapDimensions;
}
