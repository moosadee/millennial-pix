// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "UIImage.hpp"

#include <SFML/Graphics.hpp>

#include <string>

namespace chalo
{

std::string UIImage::s_className = "UIImage";

UIImage::UIImage() : IWidget()
{
}

void UIImage::Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position )
{
    sf::IntRect imageClip;
    imageClip.left = 0;
    imageClip.top = 0;

    sf::Vector2u dimensions = texture.getSize();
    imageClip.width = dimensions.x;
    imageClip.height = dimensions.y;

    Setup( key, texture, position, imageClip );
}

void UIImage::Setup( const std::string& key, const sf::Texture& texture, sf::Vector2f position, sf::IntRect imageClip )
{
    IWidget::Setup( key );

    m_sprite.setTexture( texture );
    m_sprite.setPosition( position );
    m_sprite.setTextureRect( imageClip );
}

void UIImage::SetPosition( const sf::Vector2f& pos )
{
    m_sprite.setPosition( pos );
}

void UIImage::Move( sf::Vector2f amount )
{
    sf::Vector2f pos = m_sprite.getPosition();
    m_sprite.setPosition( pos.x + amount.x, pos.y + amount.y );
}

sf::Sprite& UIImage::GetSprite()
{
    return m_sprite;
}

sf::IntRect UIImage::GetImageClipRect()
{
    return m_sprite.getTextureRect();
}

void UIImage::SetImageClipRect( sf::IntRect imageClip )
{
    m_sprite.setTextureRect( imageClip );
}

}
