// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#ifndef _UISHAPE_HPP
#define _UISHAPE_HPP

#include <string>

#include <SFML/Graphics.hpp>

#include "IWidget.hpp"

namespace chalo
{

class UIRectangleShape : public IWidget
{
public:
    UIRectangleShape();
    virtual ~UIRectangleShape()
    {
        ;
    }

    void Setup( const std::string& key, sf::RectangleShape shape );
    void Setup( const std::string& key, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size );
    void SetFillColor( sf::Color color );

    sf::RectangleShape GetShape();
protected:
    sf::RectangleShape m_shape;

private:
    static std::string s_className;
};

}

#endif

