// Chalo Engine, Moosader 2019-2020, https://gitlab.com/RachelWilShaSingh/chalo-engine

#include "UILabel.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Managers/FontManager.hpp"
#include "../Utilities/Logger.hpp"

#include <string>

namespace chalo
{

std::string UILabel::s_className = "UILabel";

UILabel::UILabel() : IWidget()
{
}

void UILabel::Setup( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text )
{
    Logger::StackPush( s_className + "::" + __func__ );
    IWidget::Setup( key );
    SetFont( fontName );
    m_text.setString( text );
    m_text.setCharacterSize( characterSize );
    m_text.setFillColor( fillColor );
    m_text.setPosition( position );
    Logger::StackPop();
}

void UILabel::SetupW( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::wstring text )
{
    Logger::StackPush( s_className + "::" + __func__ );
    IWidget::Setup( key );
    SetFont( fontName );
    m_text.setString( text );
    m_text.setCharacterSize( characterSize );
    m_text.setFillColor( fillColor );
    m_text.setPosition( position );
    Logger::StackPop();
}

void UILabel::SetFont( const std::string& fontName )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Set font \"" + fontName + "\"", s_className + "::" + __func__ );
    SetOriginalFont( fontName );
    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        if ( m_originalFont.find( "-od" ) == std::string::npos )
        {
            SetActualFont( fontName + "-od" );
        }
    }
    else
    {
        SetActualFont( fontName );
    }
    Logger::StackPop();
}

void UILabel::SetActualFont( const std::string& fontName )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Set actual font \"" + fontName + "\"", s_className + "::" + __func__ );
    sf::Font& font = chalo::FontManager::Get( fontName );
    m_text.setFont( chalo::FontManager::Get( fontName ) );
    Logger::StackPop();
}

void UILabel::SetPosition( const sf::Vector2f& pos )
{
    m_text.setPosition( pos );
}

sf::Vector2f UILabel::GetPosition()
{
    return m_text.getPosition();
}

void UILabel::Move( sf::Vector2f amount )
{
    sf::Vector2f pos = m_text.getPosition();
    m_text.setPosition( pos.x + amount.x, pos.y + amount.y );
}

sf::Text& UILabel::GetText()
{
    return m_text;
}

void UILabel::SetText( const std::string& newText )
{
    Logger::StackPush( s_className + "::" + __func__ );
    m_text.setString( newText );
    Logger::StackPop();
}

void UILabel::SetOriginalFont( const std::string& fontName )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Out( "Set original font to \"" + fontName + "\"", s_className + "::" + __func__ );
    m_originalFont = fontName;
    Logger::StackPop();
}

void UILabel::ToggleDyslexiaFont()
{
    Logger::StackPush( s_className + "::" + __func__ );
    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        if ( m_originalFont.find( "-od" ) == std::string::npos )
        {
            SetActualFont( m_originalFont + "-od" );
        }
    }
    else
    {
        SetActualFont( m_originalFont );
    }
    Logger::StackPop();
}

void UILabel::SetColor( sf::Color color )
{
    m_text.setFillColor( color );
}

}
