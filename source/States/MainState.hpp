#ifndef _MAINSTATE_HPP
#define _MAINSTATE_HPP

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/UI/UIImage.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class MainState : public chalo::IState
{
public:
    MainState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderTexture& window );

private:
    static std::string s_className;
};

#endif
