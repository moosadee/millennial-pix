#include "MainState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
//#include "../chalo-engine/Managers/ConfigManager.hpp"
//#include "../chalo-engine/Managers/LanguageManager.hpp"
//#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

std::string MainState::s_className = "MainState";

MainState::MainState()
{
}

void MainState::Init( const std::string& name )
{
    Logger::StackPush( s_className + "::" + __func__ );
    Logger::Debug( "Parameters - name: " + name, s_className + "::" + __func__ );
    Logger::StackPop();
}

void MainState::Setup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    IState::Setup();
    chalo::InputManager::Setup();

    // Load needed assets
    chalo::TextureManager::AddAndGet( "background", "Content/Graphics/UI/background.png" );
    chalo::TextureManager::AddAndGet( "button_bg", "Content/Graphics/UI/button_bg.png" );
    chalo::TextureManager::AddAndGet( "topbar_button_bg", "Content/Graphics/UI/topbar_button_bg.png" );
    chalo::TextureManager::AddAndGet( "tools_sheet", "Content/Graphics/UI/tools_sheet.png" );
//    chalo::TextureManager::AddAndGet( "desktop_icons", "Content/Graphics/UI/desktop_icons.png" );

    // Load menu file
    chalo::MenuManager::LoadTextMenu( "main.chalomenu" );

    Logger::StackPop();
}

void MainState::Cleanup()
{
    Logger::StackPush( s_className + "::" + __func__ );
    chalo::MenuManager::Cleanup();
    Logger::StackPop();
}

void MainState::Update()
{
    chalo::MenuManager::Update();
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnPlay" )
    {
        SetGotoState( "worldSelectState" );
    }
    else if ( clickedButton == "btnMapEditor" )
    {
        SetGotoState( "mapEditorState" );
    }
    else if ( clickedButton == "btnOptions" )
    {
        SetGotoState( "optionsState" );
    }
    else if ( clickedButton == "btnHelp" )
    {
        SetGotoState( "helpState" );
    }
    else if ( clickedButton == "btnExit" )
    {
        chalo::Application::ReadyToQuit();
    }
}

void MainState::Draw( sf::RenderTexture& window )
{
    chalo::MenuManager::Draw( window );
}



